package com.eva.biz.system;

import com.eva.dao.common.dto.UpdateSortDTO;
import com.eva.dao.system.model.SystemMenu;
import com.eva.dao.system.vo.SystemMenuListVO;
import com.eva.dao.system.vo.SystemMenuNodeVO;

import java.util.List;

/**
 * 系统菜单业务处理
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
public interface SystemMenuBiz {

    /**
     * 创建
     *
     * @param systemMenu 菜单
     * @return Integer
     */
    Integer create(SystemMenu systemMenu);

    /**
     * 编辑
     *
     * @param systemMenu 菜单
     */
    void updateById(SystemMenu systemMenu);

    /**
     * 菜单排序
     *
     * @param dto 详见UpdateSortDTO
     */
    void updateSort(UpdateSortDTO dto);

    /**
     * 根据用户ID查询菜单树
     *
     * @param userId 用户ID
     * @return List<SystemMenuNodeVO>
     */
    List<SystemMenuNodeVO> findTree(Integer userId);

    /**
     * 查询菜单树
     *
     * @return List<SystemMenuListVO>
     */
    List<SystemMenuListVO> findTree();

    /**
     * 删除
     *
     * @param id 菜单ID
     */
    void deleteById(Integer id);

    /**
     * 批量删除
     *
     * @param ids 菜单ID列表
     */
    void deleteByIdInBatch(List<Integer> ids);
}
