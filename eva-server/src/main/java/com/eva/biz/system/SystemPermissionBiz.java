package com.eva.biz.system;

import com.eva.dao.system.model.SystemPermission;

import java.util.List;

/**
 * 系统权限业务处理
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
public interface SystemPermissionBiz {

    /**
     * 删除
     *
     * @param id 权限ID
     */
    void deleteById(Integer id);

    /**
     * 批量删除
     *
     * @param ids 权限ID列表
     */
    void deleteByIdInBatch(List<Integer> ids);

    /**
     * 创建
     *
     * @param systemPermission 权限
     * @return Integer
     */
    Integer create(SystemPermission systemPermission);

    /**
     * 修改
     *
     * @param systemPermission 权限
     */
    void updateById(SystemPermission systemPermission);
}
