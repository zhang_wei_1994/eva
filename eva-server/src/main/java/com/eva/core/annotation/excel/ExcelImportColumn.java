package com.eva.core.annotation.excel;

import java.lang.annotation.*;

/**
 * 标记为Excel导入列
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
@Inherited
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelImportColumn {

    /**
     * 排序，值越小越靠前，-1按字段反射顺序排序
     */
    int index() default -1;

    /**
     * 数据转换器
     */
    Class<? extends ExcelDataConverterAdapter> converter() default ExcelDataConverterAdapter.class;

    /**
     * 自定义数据处理器参数
     */
    String[] args() default {};

}
