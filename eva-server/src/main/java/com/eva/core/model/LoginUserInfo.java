package com.eva.core.model;

import com.eva.dao.system.model.SystemPermission;
import com.eva.dao.system.model.SystemRole;
import com.eva.dao.system.model.SystemUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 登录用户信息
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
@Data
public class LoginUserInfo implements Serializable {

    private Integer id;

    private String username;

    private String realname;

    private String avatar;

    private Date birthday;

    private String sex;

    private List<String> roles;

    private List<String> permissions;

    @JsonIgnore
    private String password;

    @JsonIgnore
    private String salt;

    /**
     * 将用户实体转为登录用户信息
     *
     * @param user 用户实体
     * @param roles 角色列表
     * @param permissions 权限列表
     * @return LoginUserInfo
     */
    public static LoginUserInfo from(SystemUser user, List<SystemRole> roles, List<SystemPermission> permissions) {
        if (user == null) {
            return null;
        }
        // 拷贝用户信息
        LoginUserInfo loginUserInfo = new LoginUserInfo();
        BeanUtils.copyProperties(user, loginUserInfo);
        // 设置角色信息
        List<String> rs = new ArrayList<>();
        for (SystemRole role : roles) {
            rs.add(role.getCode());
        }
        loginUserInfo.setRoles(rs);
        // 设置权限信息
        List<String> pms = new ArrayList<>();
        for (SystemPermission permission : permissions) {
            pms.add(permission.getCode());
        }
        loginUserInfo.setPermissions(pms);
        return loginUserInfo;
    }
}
